json.array!(@posts) do |post|
  json.extract! post, :id, :title, :body, :comments, :post_id, :body
  json.url post_url(post, format: :json)
end
